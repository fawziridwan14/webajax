@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Product</div>
                
                <div class="panel-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                    
                    <table class="table" id="product">
                        <tr>
                            <th>ID</th>
                            <th>Nama Produk</th>
                            <th>Harga Beli</th>
                            <th>Harga Jual</th>
                            <th width="20%">Action</th>
                        </tr>
                    </table>
                    
                </div>
            </div>
        </div>
    </div>
</div>

@stop

<script>
    (function($)    {
        $(function() {
            var table = $("#product").DataTable({
            processing: true,
            serverSide: true,
            deferRender: true,
            ajax: {
            url: "{{ url('product') }}",
            method: 'GET',
            },
            
            columns: [
            { data: null, 'searchable': false , render: function (data, type, row, meta) {
            return meta.row + meta.settings._iDisplayStart + 1;
            } },
            { data: 'nama_produk', },
            { data: 'harga_beli' },
            { data: 'harga_jual' },
            ],
            scrollCollapse: true,
            
            columnDefs: [ {
            sortable: true,
            "class": "index",
            targets: 0
            }],
            order: [[ 1, 'asc' ]],
            fixedColumns: true
            
            });
            
            table.on( 'order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
            } );
            } ).draw();
            
        });        
    })
</script>