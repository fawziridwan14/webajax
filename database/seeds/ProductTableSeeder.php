<?php

use Illuminate\Support\Facades\DB;

use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use App\Product;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $faker = Faker::create();
    	foreach (range(1,50) as $index) {
            Product::create([
                'nama_produk'   => $faker->firstNameMale,
                'harga_jual'    => $faker->randomDigit,
                'harga_beli'    => $faker->randomDigit,
            ]);
        }
    }
}
